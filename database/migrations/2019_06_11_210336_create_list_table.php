<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text("title")->nullable();
            $table->text("product_description")->nullable();
            $table->text("vendor")->nullable();
            $table->text("type")->nullable();
            $table->text("tags")->nullable();
            $table->text("sku")->nullable();
            $table->text("inventory_qty")->nullable();
            $table->text("price")->nullable();
            $table->text("gift_card")->nullable();
            $table->text("cost_per_item")->nullable();
            $table->text("collection")->nullable();
            $table->text("shipping_measurements")->nullable();
            $table->text("specifications")->nullable();
            $table->text("information")->nullable();
            $table->text("recommendation")->nullable();
            $table->text("care")->nullable();
            $table->text("technical_information")->nullable();
            $table->text("measures")->nullable();
            $table->text("form")->nullable();
            $table->text("unit_per_box")->nullable();
            $table->text("color")->nullable();
            $table->text("material")->nullable();
            $table->text("business_type")->nullable();
            $table->text("country_of_origin")->nullable();
            $table->text("chipping_resistance")->nullable();
            $table->text("shock_resistance")->nullable();
            $table->text("porosity")->nullable();
            $table->text("reinforced_edges")->nullable();
            $table->text("dishwasher")->nullable();
            $table->text("microwave")->nullable();
            $table->text("conventional_oven")->nullable();
            $table->text("qty_photos")->nullable();
            $table->text("weight_unit")->nullable();
            $table->text("seo_title")->nullable();
            $table->text("seo_description")->nullable();
            $table->integer("modified")->nullable();
            $table->integer("send")->nullable();
            $table->text("published")->nullable();
            $table->text("product_id")->nullable();
            
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list');
    }
}
