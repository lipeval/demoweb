<?php

namespace App\Imports;

use App\Models\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Response;

class UploadImport implements ToCollection
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function collection(Collection $rows)
    {
        foreach($rows as $row){

            if($row[0] != 'Title'){

                Product::updateOrCreate(
                    ['sku' => $row[5]],
                    [
                        'title' => $row[0],
                        'product_description' => $row[1],
                        'vendor' => $row[2],
                        'type' => $row[3],
                        'tags' => $row[4],
                        'sku' => $row[5],
                        'inventory_qty' => 1,
                        'price' => $row[7],
                        'gift_card' => 1,
                        'weight_unit' => $row[13],
                        'cost_per_item' => $row[14],
                        'collection' => $row[15],
                        'shipping_measurements' => $row[16],
                        'specifications' => $row[17],
                        'information' => $row[18],
                        'recommendation' => $row[19],
                        'care' => $row[20],
                        'technical_information' => $row[21],
                        'measures' => $row[22],
                        'form' => $row[23],
                        'unit_per_box' => $row[24],
                        'color' => $row[25],
                        'material' => $row[26],
                        'business_type' => $row[27],
                        'country_of_origin' => $row[28],
                        'chipping_resistance' => $row[29],
                        'shock_resistance' => $row[30],
                        'porosity' => $row[31],
                        'reinforced_edges' => $row[32],
                        'dishwasher' => $row[33],
                        'microwave' => $row[34],
                        'conventional_oven'  => $row[35],
                        'qty_photos' => $row[36],
                        'seo_title' => $row[11],
                        'seo_description' => $row[12],
                        'modified' => $row[37],
                        'published' => $row[38]
                    ]
                );
            }
        }
       
    }
}
