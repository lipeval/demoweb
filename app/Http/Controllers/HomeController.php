<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\UploadImport;
use App\Models\Product;
use Excel;

require_once '../vendor/autoload.php';

class HomeController extends Controller
{	

    public function uploadFiles(Request $request){
		Excel::import(new UploadImport, $request->file('fileToUpload'));
		return redirect(route('list'));
    }

    public function __construct()
    {

    }

    public function generateDataProduct($product){
    	$images = [];
				
		for($i= 1; $i < $product->qty_photos+1 ;$i++ ){
			$images[]['src'] = 'https://cdn.shopify.com/s/files/1/0248/3994/6274/files/'.$product->sku.'_'.$i.'.jpg';
			//$images[]['src'] = 'https://cdn.shopify.com/s/files/1/0077/9688/3520/files/'.$product->sku.'_'.$i.'.jpg';
		}

    	$data = [
    		'title' => $product->title,
			'body_html' => $product->product_description,
			'vendor' => $product->vendor,
			'product_type' => $product->type,
			'tags' => $product->tags,
			'published_at' => $product->published == 'si' ? date('Y-m-d') : null,
			'metafields_global_title_tag' => $product->seo_title,
			'metafields_global_description_tag' => $product->seo_description,
			"variants" =>  [
			      [
			      	'option'=> "",
			        'price'=> floatval($product->price),
			        'sku'=> $product->sku,
			        'weight' => floatval($product->weight_unit),
			        'weight_unit' => "kg",
			        'inventory_quantity' =>  intval($product->inventory_qty),
			        'cost' => floatval($product->cost_per_item),
			        'country_code_of_origin' => 'MX',
			        'tracked' => true,
			        "inventory_management" =>  "shopify",
			        'inventory_policy' =>  'deny',
			        'inventory_item' => [
			        	'cost' => floatval($product->cost_per_item),
			        	'country_code_of_origin' => 'MX',
			        	'sku' => $product->sku,
			        	'tracked' => true
			        ],
			        'inventory_level' => [
			        	'available' => intval($product->inventory_qty)
			        ]
			      ]

			],
			"images" => $images,
			'image' => $images[0]['src'],
			"metafields" => [
				[
					"key" => "texto",
					"value" => $product->recommendation,
					"value_type" => "string",
					"namespace" => "recomendaciones"
				],
				[
					"key" => "texto",
					"value" => $product->cost_per_item,
					"value_type" => "string",
					"namespace" => "unitario"
				],
				[
					"key" => "texto",
					"value" => $product->care,
					"value_type" => "string",
					"namespace" => "cuidados"
				],
				[
					"key" => "texto",
					"value" => $product->information,
					"value_type" => "string",
					"namespace" => "informacion_linea"
				],
		     	[
					"key" => "Envio",
					"value" => $product->shipping_measurements,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Especificaciones",
					"value" => $product->specifications,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Informacion",
					"value" => $product->information,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Medida",
					"value" => $product->measures,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Forma",
					"value" => $product->form,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Unidad",
					"value" => $product->unit_per_box,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Color",
					"value" => $product->color,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Material",
					"value" => $product->material,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Tipo",
					"value" => $product->business_type,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Pais",
					"value" => $product->country_of_origin,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Resistencia1",
					"value" => $product->chipping_resistance,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Resistencia2",
					"value" => $product->shock_resistance,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Porosidad",
					"value" => $product->porosity,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Borde",
					"value" => $product->reinforced_edges,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Lavavajillas",
					"value" => $product->dishwasher,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Microondas",
					"value" => $product->microwave,
					"value_type" => "string",
					"namespace" => "ficha"
				],
				[
					"key" => "Horno",
					"value" => $product->conventional_oven,
					"value_type" => "string",
					"namespace" => "ficha"
				]
		     ]
    	];

    	return $data;
    }

    public function constructMetafields($product){


    	$data = [
			[
				"key" => "texto",
				"value" => $product->recommendation,
				"value_type" => "string",
				"namespace" => "recomendaciones"
			],
			[
				"key" => "texto",
				"value" => $product->cost_per_item,
				"value_type" => "string",
				"namespace" => "unitario"
			],
			[
				"key" => "texto",
				"value" => $product->care,
				"value_type" => "string",
				"namespace" => "cuidados"
			],
			[
				"key" => "texto",
				"value" => $product->information,
				"value_type" => "string",
				"namespace" => "informacion_linea"
			],
	     	[
				"key" => "Envio",
				"value" => $product->shipping_measurements,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Especificaciones",
				"value" => $product->specifications,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Informacion",
				"value" => $product->information,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Medida",
				"value" => $product->measures,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Forma",
				"value" => $product->form,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Unidad",
				"value" => $product->unit_per_box,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Color",
				"value" => $product->color,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Material",
				"value" => $product->material,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Tipo",
				"value" => $product->business_type,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Pais",
				"value" => $product->country_of_origin,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Resistencia1",
				"value" => $product->chipping_resistance,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Resistencia2",
				"value" => $product->shock_resistance,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Porosidad",
				"value" => $product->porosity,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Borde",
				"value" => $product->reinforced_edges,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Lavavajillas",
				"value" => $product->dishwasher,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Microondas",
				"value" => $product->microwave,
				"value_type" => "string",
				"namespace" => "ficha"
			],
			[
				"key" => "Horno",
				"value" => $product->conventional_oven,
				"value_type" => "string",
				"namespace" => "ficha"
			]
		];

    	return $data;
    }

    public function generateDataProductEdit($product){
    	$images = [];
				
		for($i= 1; $i < $product->qty_photos+1 ;$i++ ){
			$images[]['src'] = 'https://cdn.shopify.com/s/files/1/0248/3994/6274/files/'.$product->sku.'_'.$i.'.jpg';
			//$images[]['src'] = 'https://cdn.shopify.com/s/files/1/0077/9688/3520/files/'.$product->sku.'_'.$i.'.jpg';
		}

    	$data = [
    		'title' => $product->title,
			'body_html' => $product->product_description,
			'vendor' => $product->vendor,
			'product_type' => $product->type,
			'tags' => $product->tags,
			'published_at' => $product->published == 'si' ? date('Y-m-d') : null,
			'metafields_global_title_tag' => $product->seo_title,
			'metafields_global_description_tag' => $product->seo_description,
			"variants" =>  [
			      [
			      	'option'=> "",
			        'price'=> floatval($product->price),
			        'sku'=> $product->sku,
			        'weight' => floatval($product->weight_unit),
			        'weight_unit' => "kg",
			        'inventory_quantity' =>  intval($product->inventory_qty),
			        'cost' => floatval($product->cost_per_item),
			        'country_code_of_origin' => 'MX',
			        'tracked' => true,
			        "inventory_management" =>  "shopify",
			        'inventory_policy' =>  'deny',
			        'inventory_item' => [
			        	'cost' => floatval($product->cost_per_item),
			        	'country_code_of_origin' => 'MX',
			        	'sku' => $product->sku,
			        	'tracked' => true
			        ],
			        'inventory_level' => [
			        	'available' => intval($product->inventory_qty)
			        ]
			      ]

			],
			"images" => $images,
			'image' => $images[0]['src']
    	];

    	return $data;
    }

    public function getProducts(){
    	//$credential = new Slince\Shopify\PublicAppCredential('Access Token');

    	$credential = new \Slince\Shopify\PrivateAppCredential('968cd841616ab65ba9945a307de8b22b', 'ad993bbc66ddba9db04c893c21254387', '1048e532fc420ecc85ea4e5ef3ed25fa');

		$client = new \Slince\Shopify\Client($credential, 'proepta-loader.myshopify.com', [
		    'metaCacheDir' => './tmp'
		]);


    	Product::chunk(50, function ($products) use ($client){

		    foreach ($products as $product) {
				

				if($product->send == 1 && $product->modified == 1){
					if($product->product_id){
						$data = $this->generateDataProductEdit($product);
						$metaFields = $this->constructMetafields($product);

						$productGet = $client->get('products/'.$product->product_id.'/metafields');
						

						foreach($productGet['metafields'] as $get){
							$client->delete('products/'.$product->product_id.'/metafields/'.$get['id']);
						}

						$productUpdate = $client->put('products/'.$product->product_id, [
						    "product" => $data
						]);

						foreach($metaFields as $meta){
							if($meta['value']){
								$productUpdate = $client->post('products/'.$product->product_id.'/metafields', [
								    "metafield" => $meta
								]);
							}
						}
					}
				}elseif($product->send == 0 && $product->modified == 0){
					$data = $this->generateDataProduct($product);

					$productCreate = $client->post('products', [
					    "product" => $data
					]);

					$product->product_id = $productCreate['product']['id'];
					$product->send = 1;
					$product->save();
					
				}
		    }
		});
		return redirect(route('list'));
    }
}
