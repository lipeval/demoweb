<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
    	'title',
		'product_description',
		'vendor',
		'type',
		'tags',
		'sku',
		'inventory_qty',
		'price',
		'gift_card',
		'weight_unit',
		'cost_per_item',
		'collection',
		'shipping_measurements',
		'specifications',
		'information',
		'recommendation',
		'care',
		'technical_information',
		'measures',
		'form',
		'unit_per_box',
		'color',
		'material',
		'business_type',
		'country_of_origin',
		'chipping_resistance',
		'shock_resistance',
		'porosity',
		'reinforced_edges',
		'dishwasher',
		'microwave',
		'conventional_oven',
		'qty_photos',
		'seo_title',
		'seo_description',
		'send',
		'modified',
		'published'
    ];
}
